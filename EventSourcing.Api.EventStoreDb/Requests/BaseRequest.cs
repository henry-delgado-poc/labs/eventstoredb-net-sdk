﻿namespace EventSourcing.Api.EventStoreDb.Requests
{
    public class BaseRequest
    {
        /// <summary>
        /// Session Identifier
        /// </summary>
        public string CorrelationId { get; set; }
    }
}
