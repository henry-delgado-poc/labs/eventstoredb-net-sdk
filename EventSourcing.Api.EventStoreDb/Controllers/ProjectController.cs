﻿using EventSourcing.Api.EventStoreDb.DomainModels;
using EventSourcing.Api.EventStoreDb.EventStores;
using EventSourcing.Api.EventStoreDb.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EventSourcing.Api.EventStoreDb.Controllers
{
    /// <summary>
    /// Projects functionality
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ILogger<ProjectController> _logger;
        private readonly IEventStore _eventStore;
        private const string ADDED_PRODUCT = "addedProduct";
        private const string UPDATED_PRODUCT = "updatedProduct";

        private static List<Project> InMemoryProjects { get; } = new List<Project>();

        /// <summary>
        /// Projects API
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="eventStore"></param>
        public ProjectController(ILogger<ProjectController> logger, IEventStore eventStore)
        {
            _logger = logger;
            _eventStore = eventStore;
        }

        /// <summary>
        /// Create new project
        /// </summary>        
        [HttpPost]
        public async Task<IActionResult> Post(AddOrUpdateRequest<Project> request)
        {
            var project = request.Data;

            //1. Execture command to create new project
            var newProject = AddProjectCommandHandler(project);

            //2. Raising the event after we executed the command

            //2.1 Logic to get a stream name associated to project commands
            var streamName = EventsHelpers.GetStreamName(project.CompanyId.ToString(), nameof(Project), newProject.Id.ToString());

            //2.2 Raise event -> store on database
            await _eventStore.Raise(streamName, ADDED_PRODUCT, newProject);

            //for demo: not error handling or logs :) Utopia
            return Ok();
        }

        /// <summary>
        /// Update project
        /// </summary>        
        [HttpPut]
        public IActionResult Put(AddOrUpdateRequest<Project> request)
        {
            var project = request.Data;
            if (project.Id < 1)
                return BadRequest($"Missing {nameof(project.Id)}");
            if (!FoundProjectById(project.Id))
                return NoContent();
            
           //TODO: pending logic
            return Ok();
        }

        /// <summary>
        /// Adding project in memory for demo purposes. This could go on a transactional database
        /// </summary>        
        private static Project AddProjectCommandHandler(Project project)
        {
            var newProjectId = GetProductId();
            var newProject = new Project(newProjectId, project.CompanyId, project.Name, project.Building, project.SubContractorName);
            InMemoryProjects.Add(newProject);
            return newProject;
        }

        /// <summary>
        /// Searching project in memory for demo purposes. This could come from a transactional database
        /// </summary>
        /// <param name="projectId">Project ID to search</param>
        private static bool FoundProjectById(long projectId)
        {
            return InMemoryProjects != null && InMemoryProjects.Any(x => x.Id == projectId);
        }

        /// <summary>
        /// Calculating project ID for demo purpose since we are using in memory project storage
        /// </summary>
        /// <returns>(long) new project ID</returns>
        private static long GetProductId()
        {
            if (InMemoryProjects == null || !InMemoryProjects.Any())
                return 1;
            return InMemoryProjects.Select(x => x.Id).Max() + 1;
        }
    }
}
