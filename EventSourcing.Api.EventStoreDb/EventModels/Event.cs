﻿using System;

namespace EventSourcing.Api.EventStoreDb.EventModels
{
    /// <summary>
    /// The event containing the data object
    /// </summary>
    public struct Event
    {
        public long SequenceNumber { get; set; }
        public DateTimeOffset OccurredAt { get; }
        public string Name { get; }
        public object Data { get; }

        /// <summary>
        /// The event containing the data of type T
        /// </summary>
        /// <param name="sequenceNumber">Sequence Number is where the event is positioned in the ledger</param>
        /// <param name="occurredAt">Time when event was recorded</param>
        /// <param name="name">The name of the event. i.e. ProjectCreated, AnyDomainEntityCreated</param>
        /// <param name="data">The data object</param>
        public Event(long sequenceNumber, DateTimeOffset occurredAt, string name, object data)
        {
            SequenceNumber = sequenceNumber;
            OccurredAt = occurredAt;
            Name = name;
            Data = data;
        }

        /// <summary>
        /// The event containing the data of type T
        /// </summary>
        /// <param name="occurredAt">Time when event was recorded</param>
        /// <param name="name">The name of the event. i.e. ProjectCreated, AnyDomainEntityCreated</param>
        /// <param name="data">The data object</param>
        public Event(DateTimeOffset occurredAt, string name, object data)
        {
            SequenceNumber = -1;
            OccurredAt = occurredAt;
            Name = name;
            Data = data;
        }
    }
}
