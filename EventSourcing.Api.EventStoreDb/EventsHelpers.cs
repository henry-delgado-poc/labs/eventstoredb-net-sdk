﻿namespace EventSourcing.Api.EventStoreDb
{
    /// <summary>
    /// Events Helper
    /// </summary>
    public static class EventsHelpers
    {
        
        /// <summary>
        /// The logic to create stream
        /// </summary>
        /// <param name="companyId">Company owning the projects</param>
        /// <param name="domainEntityName">The domain entity name to group. i.e. Project</param>
        /// <param name="domainEntityId">The domain entity ID to group. i.e. ProjectId value of 1000</param>
        /// <returns>The stream name. For example: Company-1000-Project-2000</returns>
        public static string GetStreamName(string companyId, string domainEntityName, string domainEntityId)
        {
            return $"Company-{companyId}-{domainEntityName}-{domainEntityId}";
        }        
    }
}
