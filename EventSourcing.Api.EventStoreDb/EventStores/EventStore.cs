﻿using EventSourcing.Api.EventStoreDb.EventModels;
using EventStore.ClientAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EventSourcing.Api.EventStoreDb.EventStores
{
    /// <summary>
    /// An implementation of <see cref="IEventStore"/> using EventStoreDB
    /// </summary>
    public class EventStore : IEventStore
    {
        //private readonly IEventStoreConnection _connection;

        /// <summary>
        /// An implementation of <see cref="IEventStore"/> using EventStoreDB
        /// </summary>
        public EventStore()
        {
           // _connection = EventStoreConnection.Create(ConnectionSettings.Create().DisableTls(), new IPEndPoint(IPAddress.Loopback, 1113));
            //_connection = EventStoreConnection.Create(new IPEndPoint(IPAddress.Loopback, 2113)); //TODO: remove hardcoded port
           // _connection.ConnectAsync().Wait();
        }

        /// <summary>
        /// Gets the events based on given sequence filteting
        /// </summary>
        /// <param name="firstEventSequenceNumber"></param>
        /// <param name="lastEventSequenceNumber"></param>
        /// <returns>List of events</returns>
        public Task<IEnumerable<Event>> GetEvents(long firstEventSequenceNumber, long lastEventSequenceNumber)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// It adds events to database
        /// </summary>
        /// <param name="streamName">The stream name associated with event</param>
        /// <param name="eventName">The name of the event. i.e. addedProject</param>
        /// <param name="content">The object representing data related to this event</param>
        public async Task Raise(string streamName, string eventName, object content)
        {

            var dataJsonString = JsonConvert.SerializeObject(content, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.None });
            var jsonPayload = Encoding.UTF8.GetBytes(dataJsonString);
            var eventData = new EventData(Guid.NewGuid(), eventName, isJson: true, data: jsonPayload, metadata: null);

            //Append this to an stream in our EventStoreDb

            using (var conn = EventStoreConnection.Create("ConnectTo=tcp://admin:changeit@localhost:1113", ConnectionSettings.Create().DisableTls(),"MyConnection"))
            {
                await conn.ConnectAsync();
                WriteResult response = await conn.AppendToStreamAsync(streamName, ExpectedVersion.Any, eventData);
                Console.WriteLine("connected");
                Console.WriteLine(response.LogPosition);
            }
        }
    }
}
