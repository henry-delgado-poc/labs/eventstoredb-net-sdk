﻿using EventSourcing.Api.EventStoreDb.EventModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EventSourcing.Api.EventStoreDb.EventStores
{
    public interface IEventStore
    {
        public Task Raise(string streamName, string eventName, object content);
        public Task<IEnumerable<Event>> GetEvents(long firstEventSequenceNumber, long lastEventSequenceNumber);
    }
}
