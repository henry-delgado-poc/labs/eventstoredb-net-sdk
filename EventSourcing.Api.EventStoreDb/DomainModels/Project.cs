﻿namespace EventSourcing.Api.EventStoreDb.DomainModels
{
    /// <summary>
    /// Company Project
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Company Project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <param name="companyId">Company owning the projects</param>
        /// <param name="name">Project Name</param>
        /// <param name="building">Building Name</param>
        /// <param name="subContractorName"></param>
        public Project(long id, long companyId, string name, string building,string subContractorName)
        {
            this.Id = id;
            this.Name = name;
            this.Building = building;
            this.SubContractorName = subContractorName;
            this.CompanyId = companyId;
        }

        /// <summary>
        /// Company Project
        /// </summary>
        /// <param name="companyId">Company owning the projects</param>
        /// <param name="name">Project Name</param>
        /// <param name="building">Building Name</param>
        /// <param name="subContractorName"></param>
        public Project(long companyId, string name, string building, string subContractorName)
        {
            this.Name = name;
            this.Building = building;
            this.SubContractorName = subContractorName;
            this.CompanyId = companyId;
        }

        /// <summary>
        /// Company Project
        /// </summary>
        public Project()
        {

        }

        /// <summary>
        /// Project ID
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Project Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Building Name
        /// </summary>
        public string Building { get; set; }
        
        /// <summary>
        /// Sub Contractor Name
        /// </summary>
        public string SubContractorName { get; set; }

        /// <summary>
        /// Company owning the projects
        /// </summary>
        public long CompanyId { get; set; }
    }
}
