﻿using System;
namespace EventSourcing.Api.EventStoreDb.DomainModels
{
    /// <summary>
    /// Project Floor Penetration 
    /// </summary>
    public class Penetration
    {
        /// <summary>
        /// Penetration ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Product ID. For demo only. (might have more in real life)
        /// </summary>
        public long ProductId { get; set; }

        /// <summary>
        /// UL-System
        /// </summary>
        public long UlSystem { get; set; }
        
        /// <summary>
        /// The floor name where penetration is performed
        /// </summary>
        public string Floor { get; set; }

        /// <summary>
        /// The person performing penetration
        /// </summary>
        public string Installer { get; set; }

        /// <summary>
        /// Comments 
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// Penetration Date
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Project Floor Penetration
        /// </summary>
        /// <param name="id">Penetration ID</param>
        /// <param name="productId">Product ID</param>
        /// <param name="ulSystem">UL-System used</param>
        /// <param name="floor">Floor Name or Floor Number</param>
        /// <param name="installer">Person performing penetration</param>
        /// <param name="comments">Comments</param>
        /// <param name="date">Date when penetration was completed</param>
        public Penetration(long id, long productId, long ulSystem, string floor, string installer, string comments, DateTime date)
        {
            Id = id;
            ProductId = productId;
            UlSystem = ulSystem;
            Floor = floor;
            Installer = installer;
            Comments = comments;
            Date = date;
        }

        /// <summary>
        /// Project Floor Penetration
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="ulSystem">UL-System used</param>
        /// <param name="floor">Floor Name or Floor Number</param>
        /// <param name="installer">Person performing penetration</param>
        /// <param name="comments">Comments</param>
        /// <param name="date">Date when penetration was completed</param>
        public Penetration(long productId, long ulSystem, string floor, string installer, string comments, DateTime date)
        {
            ProductId = productId;
            UlSystem = ulSystem;
            Floor = floor;
            Installer = installer;
            Comments = comments;
            Date = date;
        }
    }
}
