﻿namespace EventSourcing.Api.EventStoreDb.DomainModels
{
    /// <summary>
    /// Image related to a project floor penetration
    /// </summary>
    public class PenetrationImage
    {
        /// <summary>
        /// Image ID
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// Was image taken before the installation?
        /// <para>true = Pre-Install image</para>
        /// <para>false = Post-Install image</para>
        /// </summary>
        public bool IsPreInstall { get; set; }

        /// <summary>
        /// The URL where the image is stored
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// The penetration ID related to the image
        /// </summary>
        public long PenetrationId { get; set; }

        /// <summary>
        /// Image related to a project floor penetration
        /// </summary>
        /// <param name="id">Image ID</param>
        /// <param name="isPreInstall">Indicates whether the image was taken on pre-installation</param>
        /// <param name="imageUrl">Image URL</param>
        /// <param name="penetrationId">Penetation related to image</param>
        public PenetrationImage(long id, bool isPreInstall, string imageUrl, long penetrationId)
        {
            Id = id;
            IsPreInstall = isPreInstall;
            ImageUrl = imageUrl;
            PenetrationId = penetrationId;
        }

        /// <summary>
        /// Image related to a project floor penetration
        /// </summary>
        /// <param name="isPreInstall">Indicates whether the image was taken on pre-installation</param>
        /// <param name="imageUrl">Image URL</param>
        /// <param name="penetrationId">Penetation related to image</param>
        public PenetrationImage(bool isPreInstall, string imageUrl, long penetrationId)
        {
            IsPreInstall = isPreInstall;
            ImageUrl = imageUrl;
            PenetrationId = penetrationId;
        }
    }
}
