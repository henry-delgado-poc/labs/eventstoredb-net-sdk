# EventStoreDB - .NET SDK 
 Database designed for Event Sourcing. Available as both Open-Source and Enterprise Versions. Uses [gRPC protocol](https://grpc.io/), and includes various SDKs (.Net, Java, Haskell). Provides Docker support. It can run as single node or highly available cluster.

EventStoreDB is an industrial-strength Event Sourcing database that stores your critical data in streams of immutable events. It was built from the ground up for Event Sourcing and offers an unrivaled solution for building event-sourced systems.

## EventStoreDB on Docker 
* [See Documentation](https://developers.eventstore.com/server/v20/server/installation/docker.html)
  
EventStoreDB has a Docker image available for any platform that supports Docker.
Running  EventStoreDB as single node without SSL:
* Go to folder containing yaml file
* ``` docker-compose up ```
* Navigate to ``` http://localhost:2113/ ```

## Service Model | Platforms | SDKs
* Open Source & Enterprise versions
* On most platforms or through Event Store Cloud
* Protocols: [gRPC protocol](https://grpc.io/), TCP (TO BE phased out), and HTTP based on the [AtomPub protocol](https://tools.ietf.org/html/rfc5023).
* [SDKs available](https://developers.eventstore.com/server/v20/server/introduction/clients.html#grpc-protocol) | [.NET Client](https://github.com/EventStore/EventStore-Client-Dotnet)

## Limitations on Projections (reactive streams)
Documentation found on books, blogs and technology sites recommend using streams on event sourcing. This is to separate and categorize content but also it could (if done well) be more efficient for searchig. 

Projections are good at solving one specific query type, a category know

HOWEVER:
There are some limitations here. Creating dynamic and as many streams is not possible as recommended for this strategy from the theoretical perspective.

### Event Store Limitations:
Streams where projections emit events cannot be used to append events from applications. When this happens, the projection will detect events not produced by the projection itself and it will break.

The reason projections exclusively own their streams is because otherwise they would lose all predictability. The projection would no longer have any idea what should be in that stream. For example, when a projection starts up from a checkpoint, it first goes through all the events after that checkpoint and checks them against the emitted stream. By doing this, the projection can understand if it up to the last event and can continue from where it left off. On top of that, the projection can verify that everything is in order, no events missing, etc. If anyone can append to the emitted streams, then the projection would have no idea where it got to last in terms of processing. Therefore, it can no longer trust that the projection itself emitted that event or if something else did.

## Links
* [EventStoreDb - Open Source with Enterprise Support](https://developers.eventstore.com/server/v20/server/introduction/#getting-started)
* [Request Support Plan - No price online](https://www.eventstore.com/support)
* [Event Sourcing and .NET > Great Video Explanations](https://youtu.be/t2AI9hODJ2E)
* [Cloud EventStoreDB in AWS](https://developers.eventstore.com/cloud/provision/aws/#create-a-network)
